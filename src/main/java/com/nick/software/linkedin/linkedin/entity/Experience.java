package com.nick.software.linkedin.linkedin.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Data
public class Experience  extends BaseEntity{
    @Size(min = 2, max = 50, message = "Incorrect format")
    private String position;

    @Size(min = 2, max = 50, message = "Incorrect format")
    private String companyName;

    @Size(min = 20, max = 500, message = "Incorrect format")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_details_id", nullable = false)
    private UserDetails userDetails;
}
