package com.nick.software.linkedin.linkedin.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Data
public class UserDetails extends BaseEntity {
    @Email(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$")
    private String phone;

    @Max(value = 100)
    private String extraContactInfo;

    @Size(min = 2, max = 20, message = "Incorrect format")
    private String mainInfo;

    @Size(min = 10, max = 400, message = "Incorrect format")
    private String education;

    @Size(min = 10, max = 500, message = "Incorrect format")
    private String technologies;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userDetails")
    private Set<Experience> experiences;

    @OneToOne(fetch = FetchType.LAZY,mappedBy = "userDetails")
    private User user;
}
