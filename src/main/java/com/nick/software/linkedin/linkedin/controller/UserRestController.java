package com.nick.software.linkedin.linkedin.controller;

import com.nick.software.linkedin.linkedin.dto.UserRegisterDto;
import com.nick.software.linkedin.linkedin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserRestController {
    @Autowired
    private UserService userService;

    @PostMapping()
    public ResponseEntity<?> createUser(@RequestBody UserRegisterDto userRegisterDto){
        try {
            userService.createUser(userRegisterDto);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    }
}
