package com.nick.software.linkedin.linkedin.entity;

import com.nick.software.linkedin.linkedin.entity.graph.ResumeGraph;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Data
@Table(name = "users")
@ResumeGraph
public class User extends BaseEntity {
    //@Size(min = 5, max = 20, message = "Incorrect format")
    //@Column(unique = true, nullable = false)
    private String username;

    //@Size(min = 2, max = 20, message = "Incorrect format")
    //@Column(nullable = false)
    private String firstName;

    //@Size(min = 2, max = 20, message = "Incorrect format")
    //@Column(nullable = false)
    private String lastName;

    //@Email(regexp = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")
    //@Column(unique = true, nullable = false)
    private String email;

    //@Min(value = 12, message = "Password is too short")
    //@Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_details_id", referencedColumnName = "id")
    private UserDetails userDetails;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Post> posts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Comment> comments;
}
