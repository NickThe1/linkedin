package com.nick.software.linkedin.linkedin.repository;

import com.nick.software.linkedin.linkedin.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
}
