package com.nick.software.linkedin.linkedin.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRegisterDto {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
