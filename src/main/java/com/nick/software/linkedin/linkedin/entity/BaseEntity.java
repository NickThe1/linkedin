package com.nick.software.linkedin.linkedin.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@Data
public class BaseEntity {
    private @Id @GeneratedValue Long id;

    @CreatedDate
    private Date created;

    @LastModifiedDate
    private Date lastModified;
}
