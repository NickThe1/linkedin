package com.nick.software.linkedin.linkedin.dto.mapper;

import com.nick.software.linkedin.linkedin.dto.UserRegisterDto;
import com.nick.software.linkedin.linkedin.entity.User;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserRegisterMapper {
    UserRegisterMapper INSTANCE = Mappers.getMapper(UserRegisterMapper.class);

    User userRegisterDtoToUser(UserRegisterDto userRegisterDto);
}
