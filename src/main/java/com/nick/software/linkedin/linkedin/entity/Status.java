package com.nick.software.linkedin.linkedin.entity;

public enum Status {
    ACTIVE, NONACTIVE, DELETED
}
