package com.nick.software.linkedin.linkedin.entity.graph;

import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;

@NamedEntityGraph(
        name = "resume",
        attributeNodes = {
                @NamedAttributeNode("firstName"),
                @NamedAttributeNode("lastName"),
                @NamedAttributeNode("email"),
                @NamedAttributeNode(value = "userDetails", subgraph = "user-details")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "user-details",
                        attributeNodes = {
                                @NamedAttributeNode("phone"),
                                @NamedAttributeNode("extraContactInfo"),
                                @NamedAttributeNode("mainInfo"),
                                @NamedAttributeNode("education"),
                                @NamedAttributeNode("technologies"),
                                @NamedAttributeNode(value = "experiences", subgraph = "project"),
                        }
                ),
                @NamedSubgraph(
                        name = "project",
                        attributeNodes = {
                                @NamedAttributeNode("position"),
                                @NamedAttributeNode("companyName"),
                                @NamedAttributeNode("description")
                        }
                )
        }
)
public @interface ResumeGraph {
}
