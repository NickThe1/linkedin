package com.nick.software.linkedin.linkedin.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Data
public class Post extends BaseEntity{
    @Size(min = 2, max = 50, message = "Incorrect format")
    private String title;

    @Size(min = 2, max = 500, message = "Incorrect format")
    private String description;

    @Size(min = 50, max = 10_000, message = "Incorrect format")
    private String body;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
    private Set<Comment> comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
