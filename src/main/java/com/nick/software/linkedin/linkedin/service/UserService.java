package com.nick.software.linkedin.linkedin.service;

import com.nick.software.linkedin.linkedin.dto.UserRegisterDto;
import com.nick.software.linkedin.linkedin.dto.mapper.UserRegisterMapper;
import com.nick.software.linkedin.linkedin.entity.User;
import com.nick.software.linkedin.linkedin.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;

    public void createUser(UserRegisterDto userRegisterDto){
        User user = UserRegisterMapper.INSTANCE.userRegisterDtoToUser(userRegisterDto);
        userRepo.save(user);
    }
}
